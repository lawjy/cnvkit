"""

说明： 通过在相似的gc bin或相似的间隔大小上窗口平均来纠正有gc偏差的bin。
    1. 将bin 的次序打乱，并按gc含量重新排序。 函数：np.random.permutation()函数，打乱次序， np.argsort(sort_key, kind='mergesort') 函数，对sort_key 值进行排序并输出index 值。

    2. rolling_median() 方法对按照gc含量排序的bin深度log值进行矫正。详细如下：
        2.1 对已排序bin的前后补充wing个bin的log 值
            2.1.1 wing :将小数或bin绝对宽度转换为半-宽度(整数) : 函数：int(math.ceil(len(x) * width * 0.5))  (width is either a fraction between 0 and 1)

            2.1.2 补充wing个bin的log值计算： 函数：np.concatenate((x[wing-1::-1],x,x[:-wing-1:-1]))

            2.1.3 这里还有两个可选择项：阈值frac, 权重：weight

        2.2 划窗取window内数据的中位值 函数：DataFrame.rolling(2 * wing + 1, 1, center=True).median()

        2.3 将bin深度log值与 步骤2.2 求得的每个区域的中位值相减，得到GC矫正后的bin深度log值
"""



cnarr = fix.center_by_window(cnarr, .1, ref_columns['gc'])

def center_by_window(cnarr, fraction, sort_key):
    """Smooth out biases according to the trait specified by sort_key.

    E.g. correct GC-biased bins by windowed averaging across similar-GC
    bins; or for similar interval sizes.
    """
    # Separate neighboring bins that could have the same key
    # (to avoid re-centering actual CNV regions -- only want an independently
    # sampled subset of presumably overall-CN-neutral bins)
    df = cnarr.data.reset_index(drop=True)
    np.random.seed(0xA5EED)
    shuffle_order = np.random.permutation(df.index)
    #df = df.reindex(shuffle_order)
    df = df.iloc[shuffle_order]
    # Apply the same shuffling to the key array as to the target probe set
    if isinstance(sort_key, pd.Series):
        # XXX why
        sort_key = sort_key.values
    sort_key = sort_key[shuffle_order]
    # Sort the data according to the specified parameter
    order = np.argsort(sort_key, kind='mergesort')
    df = df.iloc[order]
    biases = smoothing.rolling_median(df['log2'], fraction)
    # biases = smoothing.savgol(df['log2'], fraction)
    df['log2'] -= biases
    fixarr = cnarr.as_dataframe(df)
    fixarr.sort()
    return fixarr


def check_inputs(x, width, as_series=True, weights=None):
    """Transform width into a half-window size.

    `width` is either a fraction of the length of `x` or an integer size of the
    whole window. The output half-window size is truncated to the length of `x`
    if needed.
    """
    x = np.asfarray(x)
    wing = _width2wing(width, x)
    signal = _pad_array(x, wing)
    if as_series:
        signal = pd.Series(signal)
    if weights is None:
        return x, wing, signal

    weights = _pad_array(weights, wing)
    # Linearly roll-off weights in mirrored wings
    weights[:wing] *= np.linspace(1/wing, 1, wing)
    weights[-wing:] *= np.linspace(1, 1/wing, wing)
    if as_series:
        weights = pd.Series(weights)
    return x, wing, signal, weights


def _width2wing(width, x, min_wing=3):
    """Convert a fractional or absolute width to integer half-width ("wing").
    """
    if 0 < width < 1:
        wing = int(math.ceil(len(x) * width * 0.5))
    elif width >= 2 and int(width) == width:
        # Ensure window width <= len(x) to avoid TypeError
        width = min(width, len(x) - 1)
        wing = int(width // 2)
    else:
        raise ValueError("width must be either a fraction between 0 and 1 "
                         "or an integer greater than 1 (got %s)" % width)
    wing = max(wing, min_wing)
    wing = min(wing, len(x) - 1)
    assert wing >= 1, "Wing must be at least 1 (got %s)" % wing
    return wing





def _pad_array(x, wing):
    """Pad the edges of the input array with mirror copies."""
    return np.concatenate((x[wing-1::-1],
                           x,
                           x[:-wing-1:-1]))


def rolling_median(x, width):
    """Rolling median with mirrored edges."""
    x, wing, signal = check_inputs(x, width)
    rolled = signal.rolling(2 * wing + 1, 1, center=True).median()
    # if rolled.hasnans:
    #     rolled = rolled.interpolate()
    return np.asfarray(rolled[wing:-wing])


