from itertools import islice
import pysam










#####
说明： 验证bam 是否排序，按query name 还是position 排序；
思路：
    1, 从bam文件取少量的数据，
        处理：
            1. pysam 模块专门处理bam, pysam.Samfile()函数，读取bam 文件返回迭代器。
            2. 由islice()对迭代器做切片操作,取出前数十数据进行处理。
    2， 前后两条reads数据的对比
        处理：
            1. by query name 如果上一条read 的query name <= 下一条 read 的query name 则为排序
            2. by position 如果上一条read 的pos <= 下一条 read 的query name 则为排序



def ensure_bam_sorted(bam_fname, by_name=False, span=50):
    """Test if the reads in a BAM file are sorted as expected.

    by_name=True: reads are expected to be sorted by query name. Consecutive
    read IDs are in alphabetical order, and read pairs appear together.

    by_name=False: reads are sorted by position. Consecutive reads have
    increasing position.
    """
    if by_name:
        # Compare read IDs
        def out_of_order(read, prev):
            return not (prev is None or
                        prev.qname <= read.qname)
    else:
        # Compare read locations
        def out_of_order(read, prev):
            return not (prev is None or
                        read.tid != prev.tid or
                        prev.pos <= read.pos)




    # ENH - repeat at 50%, ~99% through the BAM
    bam = pysam.Samfile(bam_fname, 'rb')
    last_read = None
    for read in islice(bam, span):
        if out_of_order(read, last_read):
            return False
        last_read = read
    bam.close()
    return True