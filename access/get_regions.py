
"""
1. 说明：

2. 输入: 基因组文件，fasta 格式
>chr1
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
GCACAGAGAGAGAGCACAGAGAGAGAGCA
AAAAAACAGAGAGAGAGCACAGAGAGAGA
TATTACAGCACAGAGAAAAAAACAGAGAG
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
AAAAAACAGAGAGAGAAAAAAACAGAGAG
AAAAAAAAAATNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
AAAAAAAAAATATANNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNTATATGAGNNNNNNNGAGAGTATAT
NNNNNNNNNNNNNNNNNNNNNNNNTATAT
NNNNNN
>chr2
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNN
...
3. 分析处理:
    数据分两部分：  ">" header   NATCG content
    3.0 初始化，明确输出的内容，chrom = start = cursor = None
    3.1 header 处理， 1，header 可以确定chrom信息； 2. 结合start 作为flags 输出统计的内容
    3.2 context 处理，统计有效的序列区域(未被“N”掩盖的区域)。
    line 包含N
       情况一 line 全'N' ：all() 函数： 判断可迭代参数 iterable 是否全部为 'N',
            开头全'N' ： 更新cursor长度
            中间全'N' ： 返回cursor所在的结果，并初始化start=None
        情况二 line 包含N但不全为'N':  np.array() , np.where()函数确定'N'的位置
            line start不为None,前面为ATCG,后面为N:  yield log_this(chrom, run_start, cursor + n_indices[0])
            line start为None,前面为ATCG,后面为N: yield log_this(chrom, cursor, cursor + n_indices[0])
            line 前后为N, 中间ATCG；或者 line N*-[ATCG]*-N*-[ATCG]*-N*: np.diff() 找出gap 的start 和end 坐标； 迭代由zip()构建的参数
    line 不包含N：
        更新 start=0， 前提是start=None;
    更新 cursor， cursor +=lne(line)

    3.4 用关键词yeild 生成器。


"""

import logging
import numpy as np


def get_regions(fasta_fname):
    """Find accessible sequence regions (those not masked out with 'N')."""
    with open(fasta_fname) as infile:
        chrom = cursor = run_start = None
        for line in infile:
            if line.startswith('>'):
                # Emit the last chromosome's last run, if any
                if run_start is not None:
                    yield log_this(chrom, run_start, cursor)
                # Start new chromosome
                chrom = line.split(None, 1)[0][1:]
                run_start = None
                cursor = 0
                logging.info("%s: Scanning for accessible regions", chrom)
            else:
                line = line.rstrip()
                if 'N' in line:
                    if all(c == 'N' for c in line):
                        # Shortcut if the line is all N chars
                        if run_start is not None:
                            yield log_this(chrom, run_start, cursor)
                            run_start = None
                    else:
                        # Slow route: line is a mix of N and non-N chars
                        line_chars = np.array(line, dtype='c')
                        n_indices = np.where(line_chars == b'N')[0]
                        # Emit the first block of non-N chars, if any
                        if run_start is not None:
                            yield log_this(chrom, run_start, cursor + n_indices[0])
                        elif n_indices[0] != 0:
                            yield log_this(chrom, cursor, cursor + n_indices[0])
                        # Emit any short intermediate > blocks
                        gap_mask = np.diff(n_indices) > 1
                        if gap_mask.any():
                            ok_starts = n_indices[:-1][gap_mask] + 1 + cursor
                            ok_ends = n_indices[1:][gap_mask] + cursor
                            for start, end in zip(ok_starts, ok_ends):
                                yield log_this(chrom, start, end)
                        # Account for any tailing non-N chars
                        if n_indices[-1] + 1 < len(line_chars):
                            run_start = cursor + n_indices[-1] + 1
                        else:
                            run_start = None
                else:
                    if run_start is None:
                        # Start of a new run of non-N characters
                        run_start = cursor
                cursor += len(line)
        # Emit the last run if it's accessible (i.e. not a telomere)
        if run_start is not None:
            yield log_this(chrom, run_start, cursor)

def log_this(chrom, run_start, run_end):
    """Log a coordinate range, then return it as a tuple."""
    logging.info("\tAccessible region %s:%d-%d (size %d)",
                 chrom, run_start, run_end, run_end - run_start)
    return (chrom, run_start, run_end)

if __name__ == '__main__':
    fa_regions = get_regions("test_Homo_sapiens_assembly19.fasta")
    print(list(fa_regions))
