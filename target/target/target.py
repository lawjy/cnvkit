"""
#1. argparse 模块创建脚本输入参数。

#2. do_split 按给定bin的阈值，将intervals 拆分为合适的区间
    ##2.0 重点理解：如何分bin,判断每个区域的跨度能分为几个bin(可用round()函数进行四舍五入)，如果能分多个bin的话，则修改（chrom  bin_start  bin_end）生成新的区域。
    ##2.1 输入：目的区域bed文件： intervals.bed(chrom start end)，type:DataFrame.
    ##2.2 分析处理：
        ###2.2.1
        ### 情况1分析：每一行为一个区域的bed,行与行的区域没有重叠。
        ### 处理：a.用DataFrame中itertuples()方法迭代DataFrame中的每一行返回一个产生一个命名元组。（Pandas(Index=0, chromosome=1, start=43814930, end=43815033)）；
        ### b. 对每一行进行处理：判断这个区域的跨度能分为几个bin(可用round()函数进行四舍五入).处理后依旧输出元祖
        ### c. 用pd.DataFrame.from_records()函数将b.数据重新构造成DataFrame输出。（注：pd.DataFrame.from_records(data), data可为ndarray（结构化dtype），元组列表，dict或DataFrame）
        ### d. 拓展：b.的数据输出并没有硬性限制，只要能够满足创建DataFrame就行。（比如：二维列表，字典， 列表嵌套字典）
    ##2.3 输出： 拆分后的目的区域bed文件，type:DataFrame.

#3. annotate 为目的区域的bed文件，添加对应的基因信息。
    ##3.0 说明：该注释的算法类似于 bedtools intersect，核心：
    ##3.1 输入： 参考数据库：refFlat.txt (chrom start end gene ...)，type:DataFrame., 目的区域bed文件： intervals.bed(chrom start end)，type:DataFrame.
    ##3.2 分析处理：
        ### a. 参考数据库读入类型为 Dataframe, 并且按"chrom start end"的顺序进行排序（重点）。 调用到的主要函数：dframe.sort_values(['chromosome', 'start', 'end'])
        ### b. 如何找到对应的基因：根据目的区域的位置在数据库中找到对应的index，从而获得每个区域对应的基因。
        ### c. 如何找到对应的indexs: ①分组，参考数据库构建字典，迭代目的区域，最终返回：chrom, ctable(目的区域Dataframe), otable(参考数据库Dataframe),调用到的主要函数：dframe.groupby(['chromosome'], sort=False).②组内处理：通过bc将检索的范围缩小在同组的数据，在确定目的区域在参考数据库的index。
        ### d. 如何进行组内处理（核心）：1, 根据参考数据库构建mark(region_mask = np.ones(len(table), dtype=np.bool_),type=array); 2, 由目的区域的start_val 缩小范围(region_mask = (table.end.values > start_val)); 3, 通过函数np.searchsorted() 确定end_val的index(end_idx = table.start.searchsorted(end_val)；region_mask[int(end_idx):] = 0)。
        问题：为什么不知道用np.searchsorted()得到index;猜测注释到的位置（跨度）多个基因。
        调用到的主要函数：np.searchsorted(a, v, side='left', sorter=None)在数组a中插入数组v（并不执行插入操作），返回一个下标列表
        ### e.上述的步骤(注：理解顺序：d->b)均用了生成器（需要迭代列表时），大幅度的节省内存，缩短执行时间，优化总体性能，值得效仿。


"""





"""Transform bait intervals into targets more suitable for CNVkit."""
import pprint
import logging
import argparse
import numpy as np
import pandas as pd

def build_parser():
    ### 创建对象
    parser = argparse.ArgumentParser()
    ### 添加参数
    parser.add_argument('interval',
                          help="""BED or interval file listing the targeted regions.""")
    parser.add_argument('--annotate',
                          help="""Use gene models from this file to assign names to the target
                    regions. Format: UCSC refFlat.txt or ensFlat.txt file
                    (preferred), or BED, interval list, GFF, or similar.""")
    parser.add_argument('--short-names', action='store_true',
                          help="Reduce multi-accession bait labels to be short and consistent.")
    parser.add_argument('--split', action='store_true',
                          help="Split large tiled intervals into smaller, consecutive targets.")
    # Exons: [114--188==203==292--21750], mean=353 -> outlier=359, extreme=515
    #   NV2:  [65--181==190==239--12630], mean=264 -> outlier=277, extreme=364
    # Default avg_size chosen s.t. minimum bin size after split is ~= median
    parser.add_argument('-a', '--avg-size', type=int, default=200 / .75,
                          help="""Average size of split target bins (results are approximate).
                    [Default: %(default)s]""")
    parser.add_argument('-o', '--output', metavar="FILENAME",
                          help="""Output file name.""")
    ### 实例化对象
    args = parser.parse_args()
    return args
    pass


def subdivide(table, avg_size, min_size=0, verbose=False):
    return pd.DataFrame.from_records(
        _split_targets(table, avg_size, min_size, verbose),
        columns=table.columns)


def _split_targets(regions, avg_size, min_size, verbose):
    """Split large regions into smaller, consecutive regions.

    Output bin metadata and additional columns match the input dataframe.

    Parameters
    ----------
    avg_size : int
        Split regions into equal-sized subregions of about this size.
        Specifically, subregions are no larger than 150% of this size, no
        smaller than 75% this size, and the average will approach this size when
        subdividing a large region.
    min_size : int
        Drop any regions smaller than this size.
    verbose : bool
        Print a log message when subdividing a region.

    """
    for row in regions.itertuples(index=False):
        span = row.end - row.start
        if span > min_size:
            nbins = int(round(float(span) / avg_size)) or 1
            if nbins == 1:
                yield row
            else:
                bin_size = span / nbins
                bin_start = row.start
                if verbose:
                    label = (row.gene if 'gene' in regions else
                             "%s:%d-%d" % (row.chromosome, row.start, row.end))
                    logging.info("Splitting: {:30} {:7} / {} = {:.2f}"
                                 .format(label, span, nbins, bin_size))
                for i in range(1, nbins):
                    bin_end = row.start + int(i * bin_size)
                    yield row._replace(start=bin_start, end=bin_end)
                    bin_start = bin_end
                yield row._replace(start=bin_start)

### 分组处理
 ###c.
def by_shared_chroms(table, other, keep_empty=True):
    if table['chromosome'].is_unique and other['chromosome'].is_unique:
        yield table['chromosome'].iat[0], table, other
        # yield None, table, other
    else:
        other_chroms = {c: o for c, o in other.groupby(['chromosome'], sort=False)}

        for chrom, ctable in table.groupby(['chromosome'], sort=False):
            if chrom in other_chroms:
                otable = other_chroms[chrom]
                yield chrom, ctable, otable
            elif keep_empty:
                yield chrom, ctable, None



def _irange_simple(table, starts, ends, mode):
    """Slice subsets of table when regions are not nested."""
    if starts is not None and len(starts):
        if mode == 'inner':
            # Only rows entirely after the start point
            start_idxs = table.start.searchsorted(starts)
        else:
            # Include all rows overlapping the start point
            start_idxs = table.end.searchsorted(starts, 'right')
    else:
        starts = np.zeros(len(ends) if ends is not None else 1,
                            dtype=np.int_)
        start_idxs = starts.copy()

    if ends is not None and len(ends):
        if mode == 'inner':
            end_idxs = table.end.searchsorted(ends, 'right')
        else:
            end_idxs = table.start.searchsorted(ends)
    else:
        end_idxs = np.repeat(len(table), len(starts))
        ends = [None] * len(starts)

    for start_idx, start_val, end_idx, end_val in zip(start_idxs, starts,
                                                      end_idxs, ends):
        yield (slice(start_idx, end_idx), start_val, end_val)

###d.
def _irange_nested(table, starts, ends, mode):
    """Slice subsets of table when regions are nested."""
    # ENH: Binary Interval Search (BITS) or Layer&Quinlan(2015)
    assert len(starts) == len(ends) > 0

    for start_val, end_val in zip(starts, ends):
        # Mask of table rows to keep for this query region
        region_mask = np.ones(len(table), dtype=np.bool_)

        if start_val:
            if mode == 'inner':
                # Only rows entirely after the start point
                start_idx = table.start.searchsorted(start_val)
                region_mask[:int(start_idx)] = 0
            else:
                # Include all rows overlapping the start point
                region_mask = (table.end.values > start_val)

        if end_val is not None:
            if mode == 'inner':
                # Only rows up to the end point
                region_mask &= (table.end.values <= end_val)
            else:
                # Include all rows overlapping the end point
                end_idx = table.start.searchsorted(end_val)
                region_mask[int(end_idx):] = 0

        yield region_mask, start_val, end_val


def idx_ranges(table, chrom, starts, ends, mode):
    """Iterate through sub-ranges.""" vc
    assert mode in ('inner', 'outer')
    # Optional if we've already subsetted by chromosome (not checked!)
    if chrom:
        assert isinstance(chrom, str)  # ENH: accept array?
        try:
            table = table[table['chromosome'] == chrom]
        except KeyError:
            raise KeyError("Chromosome %s is not in this probe set" % chrom)
    # Edge cases
    if not len(table) or (starts is None and ends is None):
        yield table.index, None, None
    else:
        # Don't be fooled by nested bins
        if ((ends is not None and len(ends)) and
            (starts is not None and len(starts))
        ) and not table.end.is_monotonic_increasing:
            # At least one bin is fully nested -- account for it
            irange_func = _irange_nested
        else:
            irange_func = _irange_simple

        for region_idx, start_val, end_val in irange_func(table, starts, ends, mode):
            yield region_idx, start_val, end_val



def iter_slices(table, other, mode, keep_empty):
    """Yields indices to extract ranges from `table`.

    Returns an iterable of integer arrays that can apply to Series objects,
    i.e. columns of `table`. These indices are of the DataFrame/Series' Index,
    not array coordinates -- so be sure to use DataFrame.loc, Series.loc, or
    Series getitem, as opposed to .iloc or indexing directly into Numpy arrays.
    """
    for _c, bin_rows, src_rows in by_shared_chroms(other, table, keep_empty):
        if src_rows is None:
            # Emit empty indices since 'table' is missing this chromosome
            for _ in range(len(bin_rows)):
                yield Int64Index([])
        else:
            for slc, _s, _e in idx_ranges(src_rows, None, bin_rows.start,
                                          bin_rows.end, mode):
                indices = src_rows.index[slc].values
                if keep_empty or len(indices):
                    yield indices


def into_ranges(source, dest, src_col, default, summary_func=None):
    def join_strings(elems, sep=','):
        """Join a Series of strings by commas."""
        # ENH if elements are also comma-separated, split+uniq those too
        return sep.join(pd.unique(elems))

    def first_of(elems):
        """Return the first element of the input."""
        return elems[0]

    def last_of(elems):
        """Return the last element of the input."""
        return elems[-1]

    def series2value(ser):
        if len(ser) == 0:
            return default
        if len(ser) == 1:
            return ser.iat[0]
        return summary_func(ser)

    if not len(source) or not len(dest):
        return dest
    if summary_func is None:
        # Choose a type-appropriate summary function
        elem = source[src_col].iat[0]
        if isinstance(elem, (str, np.string_)):
            summary_func = join_strings
        elif isinstance(elem, (float, np.float_)):
            summary_func = np.nanmedian
        else:
            summary_func = first_of

    column = source[src_col]
    result = [series2value(column[slc])   ###b. index
              for slc in iter_slices(source, dest, 'outer', True)]
    return result

def do_target(bait_arr, annotate=None, do_short_name=False, do_split=False, avg_size=200/.75):
    # Drop zero-width regions
    tgt_arr = bait_arr.copy()
    tgt_arr = tgt_arr[tgt_arr.start != tgt_arr.end]

    if do_split:
        logging.info("Splitting large targets")
        tgt_arr = subdivide(tgt_arr, avg_size, 0)

    if annotate:
        logging.info("Applying annotations as target names")
        cols_shared = ['gene', 'accession', 'chromosome', 'strand']
        cols_rest = ['start', 'end',
                     '_start_cds', '_end_cds',
                     '_exon_count', '_exon_starts', '_exon_ends']
        colnames = cols_shared + cols_rest
        converters = None
        usecols = [c for c in colnames if not c.startswith('_')]
        # Parse the file contents
        dframe = pd.read_csv(annotate, sep='\t', header=None, na_filter=False,
                             names=colnames, usecols=usecols,
                             dtype={c: str for c in cols_shared},
                             converters=converters)
        annotation = dframe.assign(start=dframe.start - 1).sort_values(['chromosome', 'start', 'end']).reset_index(drop=True)
        #annotation = pd.read_csv(annotate, sep="\t", names=cols_shared)
        tgt_arr['gene'] = into_ranges(annotation, tgt_arr, 'gene', '-')

    pprint.pprint(tgt_arr)
    return tgt_arr


if __name__ == '__main__':
    args = build_parser()
    regions = pd.read_csv(args.interval, sep="\t", names=['chromosome', 'start', 'end'])
    tar_regions = do_target(regions, args.annotate, args.short-names, args.split, args.avg-size)
    tar_regions.to_csv(args.output, sep="\t")
    # regions = pd.read_csv('test.bed', sep="\t", names=['chromosome', 'start', 'end'])
    # do_target(regions, "refFlat.txt", do_short_name=False, do_split=True)





